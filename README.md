# PROJETO DISCIPLINA 5PSRR #

Professor: Thiago Rodrigues

Alunos: Carlos Romano, João Gabriel Gouveia, Leonardo Gomes, Reinaldo.

### Objetivo ###

Aplicação responsável por gerenciar versões de arquivos de configurações de outros programas. A aplicação será configurada para fazer verificações de tempos em tempos em arquivos definidos pelo usuário. A cada verificação de arquivo será gerado um hash do conteúdo e o hash enviado ao web service do projeto, onde passará por uma verificação de comparação (do hash do arquivo enviado com o hash do arquivo no servidor). Caso os hash não sejam iguais a aplicação *enviará todo o conteúdo do arquivo para o servidor para ser gerado um diff...*.

**OBS:** O ARQUIVO JAVA A SER EXECUTADO É O Bootstrap.java

### Argumentos de Inicialização ###

Caso queira que o programa já inicialize verificando os arquivos, execute o JAR ou o projeto no eclipse passando como argumento:

```
#!bash

startVerificator <INTERVALO EM MILLIS>
```
Exemplo:


```
#!bash

startVerificator 5000
```


O programa irá iniciar já executando as verificações dos arquivos (se existir) de 5 em 5 segundos, sem precisar clicar no botão de start.



**No terminal/CMD:**

```
#!bash


java -jar Bootstrap.jar startVerificator <INTERVALO EM MILLIS>
```


**No Eclipse:**

Run > Run Configurations... > Aba de Arguments > Em Program arguments:

```
#!bash

startVerificator <INTERVALO EM MILLIS>
```