package com.agente.repository;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.agente.exceptions.InvalidDirectoryException;
import com.agente.model.Archive;
import com.agente.util.Util;

public class ArchiveRepository implements IArchiveRepository {

	private static ArchiveRepository instance;
	
	private List<Archive> archives;
	
	private ArchiveRepository(){
		archives = new ArrayList<>();
	}
	
	public synchronized static ArchiveRepository getInstance() {
		if (instance == null) {
			instance = new ArchiveRepository();
		}
		return instance;
	}

	@Override
	public void addArchive(Archive file) {
		file.setId(genId());
		archives.add(file);
	}
	
	@Override
	public void setArchives(List<Archive> archives) {
		this.archives = archives;
	}

	@Override
	public void removeArchive(Archive file) {
		for (Archive a : archives) {
			if (file.getId() == a.getId()) {
				archives.remove(a);
			}
		}
	}

	@Override
	public void removeArchive(int id) {
		for (Archive a : archives) {
			if (id == a.getId()) {
				archives.remove(a);
			}
		}
	}

	@Override
	public List<Archive> getArchives() {
		return archives;
	}

	@Override
	public void updateArchive(Archive oldArchive, Archive newArchive) {
		// implementar
	}
	
	private int genId() {
		return archives.size() + 1;
	}

	@Override
	public File[] listFiles(String directory) throws InvalidDirectoryException {
		File folder = new File(directory);
		if (!folder.exists()) {
			throw new InvalidDirectoryException("Directory not exists");
		}
		return folder.listFiles();
	}

}
