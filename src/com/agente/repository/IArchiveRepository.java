package com.agente.repository;

import java.io.File;
import java.util.List;

import com.agente.exceptions.InvalidDirectoryException;
import com.agente.model.Archive;

public interface IArchiveRepository {

	void addArchive(Archive file);
	void setArchives(List<Archive> archives);
	void removeArchive(Archive file);
	void removeArchive(int id);
	void updateArchive(Archive oldArchive, Archive newArchive);
	List<Archive> getArchives();
	File[] listFiles(String directory) throws InvalidDirectoryException;
	
}
