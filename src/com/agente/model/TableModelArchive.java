package com.agente.model;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class TableModelArchive extends AbstractTableModel implements OnVerificationDoneListener {

	private List<Archive> files;
	private String[] cols;
	
	public TableModelArchive() {
		files = new ArrayList<>();
		cols = new String[]{"File", "Hash"};
		fireTableDataChanged();
	}
	
	public void setArchives(List<Archive> archives) {
		files = archives;
	}
	
	public void addArchive(Archive a) {
		files.add(a);
		fireTableDataChanged();
	}
	
	public void removeArchive(int row) {
		files.remove(row);
		fireTableDataChanged();
	}
	
	public Archive getArchive(int row) {
		return files.get(row);
	}
	
	@Override
	public int getColumnCount() {
		return cols.length;
	}

	@Override
	public int getRowCount() {
		return files.size();
	}

	@Override
	public Object getValueAt(int row, int col) {
		
		switch (col) {
		case 0:
			return files.get(row).getPath();
		case 1:
			return files.get(row).getLastHash();
		default:
			return files.get(row);
		}
		
	}
	
	@Override
	public String getColumnName(int column) {
		return cols[column];
	}

	@Override
	public void onVerificationDone() {
		fireTableDataChanged();
	}

}
