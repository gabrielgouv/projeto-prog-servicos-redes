package com.agente.model;

import java.io.Serializable;
import java.util.Calendar;

import com.agente.util.Util;

public class TransferContent implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private transient String url;
	
	private String servidorId;
	private String data;
	private String servicoNome;
	private String conteudo;

	public String getServidorId() {
		return servidorId;
	}

	public void setServidorId(String servidorId) {
		this.servidorId = servidorId;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}
	
	public void setData(Calendar data) {
		this.data = Util.dateToString(data.getTime());
	}

	public String getServicoNome() {
		return servicoNome;
	}

	public void setServicoNome(String servicoNome) {
		this.servicoNome = servicoNome;
	}

	public String getConteudo() {
		return conteudo;
	}

	public void setConteudo(String conteudo) {
		this.conteudo = conteudo;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
