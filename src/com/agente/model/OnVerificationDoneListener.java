package com.agente.model;

public interface OnVerificationDoneListener {

	public void onVerificationDone();
	
}
