package com.agente.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;

import com.agente.util.Util;

public class Archive implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int id;
	private String name;
	private String path;
	private long lastVerification;
	private String lastHash;
	
	public Archive(String path) {
		this.path = path;
	}

	public Archive(File file) {
		this.path = file.getAbsolutePath();
		this.name = file.getName();
		this.lastVerification = System.currentTimeMillis();
		try {
			this.lastHash = Util.getHashFromFile(path);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public long getLastVerification() {
		return lastVerification;
	}

	public void setLastVerification(long lastVerification) {
		this.lastVerification = lastVerification;
	}

	public String getLastHash() {
		return lastHash;
	}

	public void setLastHash(String lastHash) {
		this.lastHash = lastHash;
	}
}
