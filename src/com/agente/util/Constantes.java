package com.agente.util;

public class Constantes {

	public static final String VERIFICATOR_TAG = "VERIFICATOR";
	public static final String AGENTE_NAO_INICIADO = "A thread do agente nao foi iniciada";
	public static final String COMMUNICATOR_TAG = "COMMUNICATOR";
	public static final String AGENT_TAG = "AGENT";
	public static final String CONTROLLER_ARCHIEVE_TAG = "CONTROLLER_ARCHIEVE";
	public static final String URI_SERVICE = "http://serverconfapi.azurewebsites.net/api/Configuracoes";
	public static final String SERVICE_NAME = "service name";
	public static final String SERVER_IDENTIFIER = "12";
	public static final String PAR_SERVER_ID = "PAR_SERVER_ID";
}
