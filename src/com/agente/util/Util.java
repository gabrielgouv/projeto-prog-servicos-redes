package com.agente.util;
import java.io.File;
import java.io.FileNotFoundException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

public class Util {

	public static String getHashFromFile(String caminho) throws FileNotFoundException {

		try {
			
			String texto = contentFromFile(caminho);
			
			MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			messageDigest.update(texto.getBytes());
			
			return new HexBinaryAdapter().marshal(messageDigest.digest());
		
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			throw new FileNotFoundException();
		}
		
		return null;
	}
	
	
	public static String contentFromFile(String caminho) throws FileNotFoundException {
		
		StringBuffer text = new StringBuffer();
		
		Scanner scanner = new Scanner(new File(caminho));
		
		while (scanner.hasNextLine()) {
			text.append(scanner.nextLine());
		}
		
		scanner.close();
		
		return text.toString();
			
	}
	
	public static boolean debugging = java.lang.management.ManagementFactory.getRuntimeMXBean().
	        getInputArguments().toString().indexOf("-agentlib:jdwp") > 0;
	
	public static boolean isNullOrEmpty(String text) {
		return text == null || text.trim().length() == 0;
	}
	
	public static boolean objectIsNull(Object obj) {
		return obj == null;
	}
	
	public static boolean isLong(String value) {
		try {
			Long.parseLong(value);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}
	
	public static String dateToString(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSSSS");
		if (!objectIsNull(date)) {
			return formatter.format(date);
		}
		throw new RuntimeException("data inv�lida!");
	}
	
	public static String getExtensionFile(String fileName) {
		if (!isNullOrEmpty(fileName) && fileName.lastIndexOf('.') > -1) {
			return fileName.substring(fileName.lastIndexOf('.'), fileName.length());
		}
		
		return "";
	}
	
	public static boolean isNumeric(String s) {  
	    return s != null && s.matches("[-+]?\\d*\\.?\\d+");  
	}
}
