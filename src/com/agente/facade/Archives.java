package com.agente.facade;

import java.util.List;

import com.agente.model.Archive;
import com.agente.repository.ArchiveRepository;
import com.agente.repository.IArchiveRepository;

public class Archives {

	private static Archives instance;
	
	private IArchiveRepository repository = ArchiveRepository.getInstance();
	
	private Archives() {
		
	}
	
	public synchronized static Archives getInstance() {
		if (instance == null) {
			instance = new Archives();
		}
		return instance;
	}
	
	public void addArchive(Archive file) {
		repository.addArchive(file);
	}
	
	public void setArchives(List<Archive> archives) {
		repository.setArchives(archives);
	}

	public void removeArchive(Archive file) {
		repository.removeArchive(file);
	}

	public void removeArchive(int id) {
		repository.removeArchive(id);
	}
	
	public List<Archive> getArchives() {
		return repository.getArchives();
	}

	public void updateArchive(Archive oldArchive, Archive newArchive) {
		// implementar
	}
	
}
