package com.agente.database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Scanner;

import com.agente.util.Logger;

public class JSONMiniDataBase {

	private static JSONMiniDataBase instance;
	private String defaultPath;
	
	private JSONMiniDataBase() {
		defaultPath = "src/bd.json";
	}
	
	public static JSONMiniDataBase getInstance() {
		if (instance == null) {
			instance = new JSONMiniDataBase();
		}
		return instance;
	}
	
	public String getData() {
		try {

			StringBuffer sb = new StringBuffer();
			Scanner s = new Scanner(new FileReader(defaultPath));
			
			while (s.hasNext()) {
				sb.append(s.nextLine());
			}
			
			s.close();
			return sb.toString();
			
		} catch (FileNotFoundException e) {
			return null;
		}
	}
	
	public void updateData(String json) {
		try { 
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new FileOutputStream(new File(defaultPath)));
	        outputStreamWriter.write(json);
	        outputStreamWriter.close();
	        Logger.log("DATABASE", "saved.");
	    }
	    catch (IOException e) {
	        e.printStackTrace();
	    } 
	}
	
	public String getPath() {
		return new File(defaultPath).getAbsolutePath();
	}
	
}
