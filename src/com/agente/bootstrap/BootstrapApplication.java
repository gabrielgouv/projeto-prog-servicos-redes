package com.agente.bootstrap;

import java.lang.reflect.Type;
import java.util.List;

import com.agente.database.JSONMiniDataBase;
import com.agente.facade.Archives;
import com.agente.gui.Index;
import com.agente.model.Archive;
import com.agente.negocio.Config;
import com.agente.util.Constantes;
import com.agente.util.Logger;
import com.agente.util.Util;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class BootstrapApplication {

	private static final String TAG = "BOOTSTRAP";
	
	
	public static void main(String[] args) {

		startApplication();
		showWindow(args);
		
	}

	private static void showWindow(String[] args) {
		Index index = new Index();
		index.setVisible(true);
		
		if (args.length == 2) {
			if (args[0].equals("startVerificator") && Util.isLong(args[1])) {
				index.startVerificator(Long.parseLong(args[1]));
			}
		}
	}
	
	public static void startApplication() {
		
		JSONMiniDataBase db = JSONMiniDataBase.getInstance();
		Archives archives = Archives.getInstance();
		
		if (db.getData() != null) {
			
			Type listType = new TypeToken<List<Archive>>() {}.getType();
			List<Archive> archivesFromJSON = new Gson().fromJson(db.getData(), listType);
			
			archives.setArchives(archivesFromJSON);
			
			Logger.log(TAG, "Arquivos salvos recuperados!");
						
		} else {
			Logger.log(TAG, "Nenhum arquivo para ser recuperado.");
		}
		
	}

}
