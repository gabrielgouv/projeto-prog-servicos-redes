package com.agente.negocio;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class Config {
	
	public static String getParametroAgente(String nomeParametro) throws Exception {
        Properties props = new Properties();
        InputStream is = null;
        try {
            File f = new File(System.getProperty("user.home") + "/agente.properties");
            is = new FileInputStream( f );
        }
        catch ( Exception e ) { is = null; }
     
        try {
            props.load( is );
        }
        catch ( Exception e ) {
            System.out.println("ERRO INESPERADO");
            e.printStackTrace();
            throw e;
        }
     
       return props.getProperty(nomeParametro);
    }
			
}
