package com.agente.negocio;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import com.agente.model.Archive;
import com.agente.util.Constantes;
import com.agente.util.Logger;
import com.agente.util.Util;

public class ArchivesVerificator {

	private final String TAG = Constantes.AGENT_TAG;
	private List<Archive> archives;
	private List<Archive> filesModifieds = new ArrayList<Archive>();
	
	public ArchivesVerificator(List<Archive> archives) {
		this.archives = archives;
	}
	
	public List<Archive> verify() {
		
		Logger.log(TAG, "\nAnalyzing content of files...");
		Logger.logInScreen(TAG, "Analyzing content of files...");
		
		filesModifieds.clear();
		StringBuilder names = new StringBuilder();
		for (Archive a : archives) {
			String path = a.getPath();
			int id = a.getId();
			try {
				String currentHash = Util.getHashFromFile(path);
				if (!Util.isNullOrEmpty(currentHash) && !Util.isNullOrEmpty(a.getLastHash())) {
					if (!currentHash.equalsIgnoreCase(a.getLastHash())) {
						filesModifieds.add(a);
						names.append("[ " + id + " ] " + a.getName().concat("\n"));
					} 
				}
				a.setLastHash(currentHash);
				Logger.log(TAG, "[SUCESSO] Arquivo verificado: " + path + " [ HASH: " + currentHash + " ]");
				//Logger.logInScreen(TAG, "[SUCESSO] Arquivo verificado: " + path + " [ HASH: " + currentHash + " ]");
				
			} catch (FileNotFoundException e) {
				Logger.log(TAG, "[  ERRO ] Erro ao verificar o arquivo: " + path + ". O arquivo nao existe. ");
				Logger.logInScreen(TAG, "[  ERRO ] Erro ao verificar o arquivo: " + path + ". O arquivo nao existe. ");
			}
		}
		if (filesModifieds.size() == 0) {
			Logger.logInScreen(TAG, "No file changed");
			Logger.log(TAG, "No file changed");
		} else {
			Logger.logInScreen(TAG, "Files modified: \n".concat(names.toString()));
			Logger.log(TAG, "Files modified: \n".concat(names.toString()));
		}
		return filesModifieds;
	}
}
