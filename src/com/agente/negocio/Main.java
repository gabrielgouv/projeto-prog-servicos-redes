package com.agente.negocio;
import java.util.Scanner;

import com.agente.util.Util;

public class Main {

	private static Agent agente;
	
	public static void main(String[] args) {
		
//		Scanner s = new Scanner(System.in);
//		agente = new Agent();
//		
//		String comando = "";
//		
//		do {
//			comando = s.nextLine();
//			
//			menuSwitch(comando);
//			
//		} while (!comando.equals("exit"));
//		
//		s.close();
		
	}
	
	private static void menuSwitch(String comando) {
		if (comando.equals("start")) {
			agente.startAgentVerifications();
		} else if (comando.equals("stop")) {
			agente.stopAgentVerifications();
		} else if (comando.equals("update")) {	
			agente.updateVerificationInterval(1000);		
		} else if (comando.equals("cleanup")) {	
			agente.interruptAgentThread();				
		} else if (comando.equals("test")) {
			//System.out.println(Util.getHashFromFile("src/teste.txt"));	
		} else if (comando.equals("help")) {
			mostraHelp();
		} else {
			System.out.println("[ERRO] O comando inserido e invalido.");
		}
	}
	
	private static void mostraHelp() {
		StringBuffer help = new StringBuffer();
		help.append("Comandos --------- Descricao\n\n");
		help.append("init     --------- Inicializa a thread do agente\n");
		help.append("start    --------- Inicia as verificacoes dos arquivos\n");
		help.append("stop     --------- Para as verificacoes dos arquivos\n");
		help.append("cleanup  --------- Para a theard do agente");
		
		System.out.println(help);
	}
}
