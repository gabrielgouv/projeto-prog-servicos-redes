package com.agente.negocio;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.agente.util.Logger;
import com.agente.util.Util;

public abstract class Task implements Runnable {

	private final String TAG = getClass().getName();
	private ExecutorService executor;
	private Future<?> publisher;
	
	private long interval = 0;
	
	//abstract void onPreExecute();
	abstract void runInBackground();
	//abstract void onPostExecute();
	
	@Override
	public void run() {
		
		//onPreExecute();
		
		while (!Thread.currentThread().isInterrupted()) {
			try {
				runInBackground();
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}
		
		//onPostExecute();
		
	}
	
	public void start() {
		startThread();
	}
	
	public void start(long interval) {
		if (interval >= 0) {
			this.interval = interval;
			startThread();
		}	
	}
	
	private void startThread() {
		executor = Executors.newSingleThreadExecutor();
		publisher = executor.submit(this);
		Logger.log(TAG, "Thread started.");
		Logger.logInScreen(TAG, "Thread started.");
	}
	
	public void pause() {
		publisher.cancel(true);
		Logger.log(TAG, "Thread paused.");
		Logger.logInScreen(TAG, "Thread paused.");
	}
	
	public void resume() {
		start(interval);
		Logger.log(TAG, "Thread resumed.");
		Logger.logInScreen(TAG, "Thread resumed.");
	}
	
	public void stop() {
		if (!Util.objectIsNull(executor)) {
			executor.shutdownNow();
			Logger.log(TAG, "Thread stopped.");
			Logger.logInScreen(TAG, "Thread stopped.");
		}	
	}
	
	public void setInterval(long millis) {
		Logger.log(TAG, "Setting interval. Restarting thread...");
		Logger.logInScreen(TAG, "Setting interval. Restarting thread...");
		stop();
		start(millis);
		Logger.log(TAG, "Thread restarted! Interval set to: " + millis + "ms.");
		Logger.logInScreen(TAG, "Thread restarted! Interval set to: " + millis + "ms.");
	}

}
