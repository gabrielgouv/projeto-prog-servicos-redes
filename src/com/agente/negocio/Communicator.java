package com.agente.negocio;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;

import com.agente.model.TransferContent;
import com.agente.util.Constantes;
import com.agente.util.Logger;
import com.agente.util.Util;
import com.google.gson.Gson;

public final class Communicator {

	private static Communicator communicator;
	
	private Communicator() {
		
	}
	
	public static Communicator getInstance() {
		if (communicator == null) {
			communicator = new Communicator();
		}
		return communicator;
	}
	
//	private void post(TransferContent transferContent) {
//		try {
//			URLConnection conn = new URL(transferContent.getUrl()).openConnection();
//			conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//			conn.setDoOutput(true);
//
//			OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream());
//
//			writer.write(toJson(transferContent));
//
//			writer.flush();
//			writer.close();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
	
	public String post(TransferContent transferContent) {

		HttpURLConnection conn = null;
		StringBuilder response = new StringBuilder();
		
		if (transferContent != null) {
			try {
				URL url = new URL(transferContent.getUrl());
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

				OutputStream os = conn.getOutputStream();
				os.write(toJson(transferContent).getBytes());
				os.flush();
				Logger.log(Constantes.COMMUNICATOR_TAG, "request executed, waiting response from service...");
				if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
					Logger.log(Constantes.COMMUNICATOR_TAG, "Failed : HTTP error code : " + conn.getResponseCode());
					throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
				}

				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

				String output;
				while ((output = br.readLine()) != null) {
					response.append(output);
				}
				Logger.log(Constantes.COMMUNICATOR_TAG, "response from service: " + response);

				conn.disconnect();

			} catch (MalformedURLException e) {
				Logger.log(Constantes.COMMUNICATOR_TAG, "Error: " + e.getMessage() + "Cause: " + e.getCause());
				e.printStackTrace();
			} catch (IOException e) {
				Logger.log(Constantes.COMMUNICATOR_TAG, "Error: " + e.getMessage() + "Cause: " + e.getCause());
				e.printStackTrace();
			} 
		}
		
		return response.toString();
	}
	
	public static void main(String[] args) {
		TransferContent transfer = new TransferContent();
		transfer.setConteudo("conteudo do arquivo de config");
		transfer.setData(Calendar.getInstance());
		transfer.setServicoNome("Service Teste");
		transfer.setUrl("http://serverconfapi.azurewebsites.net/api/Configuracoes");
		
		String response = Communicator.getInstance().post(transfer);
		System.out.println(response);
	}
	
	private String toJson(TransferContent transfer) {
		if (!Util.objectIsNull(transfer)) {
			return new Gson().toJson(transfer);
		}
		throw new RuntimeException("object is invalid!");
	}
}
