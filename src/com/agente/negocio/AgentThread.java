package com.agente.negocio;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.agente.model.Archive;
import com.agente.util.Logger;
import com.agente.util.Util;

@Deprecated
public class AgentThread implements Runnable {

	private final String TAG = getClass().getName();
	
	private ExecutorService executor = Executors.newSingleThreadExecutor();
	private long millis;
	private boolean running;
	private List<Archive> archives;

	public AgentThread(long millis, List<Archive> archives) {
		this.millis = millis;
		this.archives = archives;
	}

	public AgentThread(List<Archive> archives) {
		Logger.log(TAG, "Usando configuracoes padroes.");
		this.millis = 5000;
		this.archives = archives;
	}
	
	@Override
	public void run() {
		
		try {
			
			Logger.log(TAG, "Agente iniciado com sucesso!");
			
			while (!Thread.currentThread().isInterrupted()) {
				
				if (running) {

					Logger.log(TAG, "Verificando os arquivos...");
					
					for (Archive a : archives) {
						// procedimentos...
						String path = a.getPath();
						try {
							Logger.log(TAG, "[SUCESSO] Arquivo verificado: " + path + " [ HASH: " + Util.getHashFromFile(path) + " ]");
						} catch (FileNotFoundException e) {
							Logger.log(TAG, "[  ERRO ] Erro ao verificar o arquivo: " + path + ". O arquivo nao existe. ");
						}
						// enviar pro servidor e obter retorno...
					}
					
					Thread.sleep(millis - 1);
				}
				
				Thread.sleep(1);
				
			}
			
		} catch (InterruptedException e) {
			
		}
		Logger.log(TAG, "A thread foi finalizada.");
		
	}

	public void start() {
		running = true;
		Logger.log(TAG, "Verificacoes dos arquivos iniciadas, intervalo entre as verificacoes configurado para " + this.millis + "ms.");
	}

	public void stop() {
		Logger.log(TAG, "Aguardando para parar as verificacoes dos arquivos...");
		running = false;
		Logger.log(TAG, "Verificacoes dos arquivos paradas");
	}
	
	public void updateInterval(long millis) {
		// Acho que poderia matar a thread e startar denovo para que o intervalo seja alterado na hora.
		Logger.log(TAG, "O intervalo de verificao foi alterado de " + this.millis + "ms para " + millis + "ms.");
		this.millis = millis;
	}
	
	public void addFile(Archive file) {
		archives.add(file);
	}
	
	private void restartThread() {
		if (!Thread.currentThread().isInterrupted()) {
			Thread.currentThread().interrupt();
			new AgentThread(archives);
		}
	}

}
