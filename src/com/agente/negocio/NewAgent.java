package com.agente.negocio;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import com.agente.controller.ControllerArchieve;
import com.agente.model.Archive;
import com.agente.model.OnVerificationDoneListener;
import com.agente.util.Constantes;
import com.agente.util.Logger;

public class NewAgent {

	private final String TAG = getClass().getName();

	private AsyncTask asyncTask;
	private ArchivesVerificator archivesVerificator;
	private ControllerArchieve controllerArchieve = new ControllerArchieve();
	
	private List<OnVerificationDoneListener> listeners;
	
	private long verificationInterval;

	public NewAgent(List<Archive> archives, long interval) {
		listeners = new ArrayList<>();
		archivesVerificator = new ArchivesVerificator(archives);
		verificationInterval = interval;
		
		asyncTask = new AsyncTask();
	}

	public void startVerificator() {
		asyncTask.start(verificationInterval);
	}
	
	public void pauseVerificator() {
		asyncTask.pause();
	}
	
	public void resumeVerificator() {
		asyncTask.resume();
	}
	
	public void stopVerificator() {
		asyncTask.stop();
	}
	
	public void setVerificationInterval(long millis) {
		asyncTask.setInterval(millis);
		
	}
	
	public void setOnVerificationDoneListener(OnVerificationDoneListener onVerificationDoneListener) {
		listeners.add(onVerificationDoneListener);
	}
	
	private void notifyListeners() {
		for (OnVerificationDoneListener l : listeners) {
			l.onVerificationDone();
		}
	}
	
	private class AsyncTask extends Task {

		@Override
		void runInBackground() {
			
			List<Archive> filesModifieds = archivesVerificator.verify();
			if (filesModifieds.size() > 0) {
				for (Archive archive: filesModifieds) {
					try {
						Logger.logInScreen(Constantes.AGENT_TAG, "Sending " + archive.getName() + " to the server...");
						String response = controllerArchieve.sendContentToService(archive);
						
						System.out.println(response);
						
					} catch (Exception e) {
						System.out.println("Nao nao");
						Logger.log(Constantes.AGENT_TAG, "Error sending file ".concat(archive.getName()) );
						Logger.logInScreen(Constantes.AGENT_TAG, "Error sending file ".concat(archive.getName()) );
					}
				}
			}
			
			notifyListeners();
			
		}
	}
}
