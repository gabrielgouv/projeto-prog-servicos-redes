package com.agente.negocio;

import java.util.List;

import com.agente.model.Archive;
import com.agente.util.Constantes;
import com.agente.util.Logger;

@Deprecated
public class Agent {

	private final String TAG = getClass().getName();
	
	private List<Archive> archives;
	private AgentThread agentThread;
	private Thread thread;
		
	public Agent(List<Archive> archives) {
		this.archives = archives;
		init();
	}
	
	private void init() {
		if (agentThread == null) {
			agentThread = new AgentThread(archives);
			thread = new Thread(agentThread);
			thread.start();
		} else {
			Logger.log(TAG, "O agente ja esta inicializado!");
		}
	}
	
	public void startAgentVerifications() {
		if (agentThread != null) {
			agentThread.start();
		} else {
			throw new RuntimeException(Constantes.AGENTE_NAO_INICIADO);
		}
		
		
	}

	public void stopAgentVerifications() {
		if (agentThread != null) {
			agentThread.stop();
		} else {
			throw new RuntimeException(Constantes.AGENTE_NAO_INICIADO);
		}
		
	}
	
	public void updateVerificationInterval(long millis) {
		if (agentThread != null) {
			agentThread.updateInterval(millis);
		} else {
			throw new RuntimeException(Constantes.AGENTE_NAO_INICIADO);
		}
	}
	
	public void interruptAgentThread() {
		thread.interrupt();
	}
}
