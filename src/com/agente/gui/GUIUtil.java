package com.agente.gui;

import java.awt.Color;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.DefaultCaret;

public class GUIUtil {

	private GUIUtil() {
		
	}
	
	public static JTextField createTextField(int x, int y, int width, int height) {
		JTextField textField = new JTextField();
		textField.setBounds(x, y, width, height);
		return textField;
	}
	
	public static JButton createButton(String label, int x, int y, int width, int height) {
		JButton btn = new JButton(label);
		btn.setBounds(x, y, width, height);
		return btn;
	}
	
	public static JLabel createLabel(String label, int x, int y, int width, int height, JPanel panel) {
		JLabel jLabel = new JLabel(label);
		jLabel.setBounds(x, y, width, height);
		panel.add(jLabel);
		
		return jLabel;
	}
	
	public static JTextArea createJtextArea(JTextArea jArea, boolean editable, int x, int y, int width, int height, JPanel panel) {
		jArea = new JTextArea("");
		jArea.setLineWrap(true);
		jArea.setEditable(editable);
		jArea.setBackground(Color.BLACK);
		jArea.setForeground(Color.WHITE);
		DefaultCaret caret = (DefaultCaret) jArea.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		JScrollPane scroll = new JScrollPane(jArea);
		scroll.setBounds(x, y, width, height);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		panel.add(scroll);
		return jArea;
	}
	
	public static void populateJComboBox(JComboBox<String> combo, Set<String> options) {
		
		Iterator<String> it = options.iterator();
		while (it.hasNext()) {
			combo.addItem(it.next());
		}
	}
	
	public static AbstractTableModel createJTable(JTable jTable, AbstractTableModel model, int x, int y, int width, int height, JPanel panel) {
		
		jTable.setModel(model);

		JScrollPane scroll = new JScrollPane();
		scroll.setViewportView(jTable);
		scroll.setBounds(x, y, width, height);
		
		panel.add(scroll);
		
		return model;
		
	}
	
	public static File callJFileChooser(FrameBase window) {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int result = fileChooser.showOpenDialog(window);
		
		if (result == JFileChooser.APPROVE_OPTION) {
			return fileChooser.getSelectedFile();
		}
		
		return null;
	}
	
}
