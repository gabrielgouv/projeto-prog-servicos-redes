package com.agente.gui;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class FrameBase extends JFrame {

	protected void showMessage(String message) {
		JOptionPane.showMessageDialog(null, message);
	}
}
