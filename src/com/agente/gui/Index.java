package com.agente.gui;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import com.agente.bootstrap.BootstrapApplication;
import com.agente.controller.ControllerArchieve;
import com.agente.database.JSONMiniDataBase;
import com.agente.exceptions.InvalidDirectoryException;
import com.agente.facade.Archives;
import com.agente.model.Archive;
import com.agente.model.TableModelArchive;
import com.agente.negocio.Config;
import com.agente.negocio.NewAgent;
import com.agente.util.Constantes;
import com.agente.util.Logger;
import com.agente.util.Util;
import com.google.gson.Gson;

public class Index extends FrameBase {

	private final String TAG = "UI";

	private JPanel panel;
	
	private JTextField watchedDirectory;
	
	private JButton btnStart;

	private JButton btnStop;
	
	private JButton btnUpdate;
	
	private JButton btnCleanUp;

	private JButton btnSrcExtensions;
	
	private TableModelArchive tableModel;
	
	private JTable fileTable;
	
	private JScrollPane tablePane;
	
	private JTextArea textArea;
	
	private JButton btnStatus;
	
	private JButton btnSelectOption;
	
	private ControllerArchieve controllerArchieve = ControllerArchieve.getInstance();
	
	private JSONMiniDataBase db;
	
	private JComboBox<String> comboExtensions;
	
	private JLabel lblSelectedsOptions;
	
	private JTextField txtInterval; 
	
	private NewAgent agent;

	private JButton btnRemoveFile;

	public Index() {
		
		db = JSONMiniDataBase.getInstance();		
		this.setTitle("Agente");
		this.setSize(700, 700);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);  
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		panel = new JPanel(null);
		start();
	}
	
	private void start() {

		
		btnSrcExtensions = GUIUtil.createButton("Add File", 330, 115, 150, 22);
		addEventSearchExtensions(btnSrcExtensions);
		panel.add(btnSrcExtensions);
		
		btnRemoveFile = GUIUtil.createButton("Remove selected file", 480, 115, 200, 22);
		addEventRemoveSelectedFile(btnRemoveFile);
		panel.add(btnRemoveFile);
		
		GUIUtil.createLabel("Server ID:", 10, 40, 80, 22, panel);
		
		try {
			String id = Config.getParametroAgente(Constantes.PAR_SERVER_ID);
			GUIUtil.createLabel(id, 90, 40, 400, 22, panel);
			
		} catch (Exception e1) {
			GUIUtil.createLabel("Nao configurado", 90, 40, 400, 22, panel);
		}
		
		
		GUIUtil.createLabel("Interval (ms): ", 10, 80, 100, 22, panel);
		txtInterval = GUIUtil.createTextField(120, 80, 170, 22);
		panel.add(txtInterval);
		
		GUIUtil.createLabel("Files: ", 10, 115, 80, 22, panel);

		fileTable = new JTable();
		tableModel = new TableModelArchive();
		tableModel = (TableModelArchive) GUIUtil.createJTable(fileTable, tableModel, 10, 145, 673, 180, panel);
		tableModel.setArchives(Archives.getInstance().getArchives());
		
		GUIUtil.createLabel("Log: ", 10, 330, 80, 22, panel);
		textArea = GUIUtil.createJtextArea(textArea, false, 10, 360, 673, 230, panel);
		Logger.setFieldLog(textArea);
		
		GUIUtil.createLabel("Status: ", 10, 600, 80, 22, panel);
		btnStatus = GUIUtil.createButton("", 60, 606, 12, 12);
		btnStatus.setBackground(Color.RED);
		panel.add(btnStatus);

		btnStart = GUIUtil.createButton("Start", 5, 640, 170, 22);
		btnStart.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				if (!validateData()) {
					return;
				}
				
				startService();
				
			}
		});
		panel.add(btnStart);
		
		btnStop = GUIUtil.createButton("Stop", 260, 640, 170, 22);
		btnStop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				btnStatus.setBackground(Color.RED);
				if (!Util.objectIsNull(agent)) {
					agent.stopVerificator();
				}
			}
		});
		
		panel.add(btnStop);
		
		btnCleanUp = GUIUtil.createButton("Resume", 515, 640, 170, 22);
		panel.add(btnCleanUp);
		
		btnCleanUp.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				agent.resumeVerificator();
				
			}
		});
		
		
		this.add(panel);
	}
	
	private void addEventBtnAddExtension(JButton btnAdd) {

		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				
				String item = (String) comboExtensions.getSelectedItem();
				if (!Util.isNullOrEmpty(item)) {
					lblSelectedsOptions.setText(lblSelectedsOptions.getText().trim().length() > 0 ? lblSelectedsOptions.getText().concat(" ; ".concat(item)) : item);
					comboExtensions.removeItem(item);
				}
			}
		});
	}
	
	private boolean validateData() {
		//String directory = watchedDirectory.getText();
		String interval = txtInterval.getText();
		//String extensions = lblSelectedsOptions.getText();
//		String errors = controllerArchieve.validateData(interval, directory, extensions);
//		if (!Util.isNullOrEmpty(errors)) {
//			showMessage(errors);
//			return false;
//		}
		return true;
	}
	
	private void addEventSearchExtensions(JButton btnSearch) {
		btnSearch.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				File file = GUIUtil.callJFileChooser(Index.this);
				
				Archive a = new Archive(file);
				a.setLastHash("Not verified yet");
				
				Archives archives = Archives.getInstance();
				archives.addArchive(a);
				tableModel.fireTableDataChanged();
				
				String json = new Gson().toJson(archives.getArchives()).toString();
				db.updateData(json);
				
			}
		});
	}
	
	private void addEventRemoveSelectedFile(JButton btnSearch) {
		btnSearch.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
			
				tableModel.removeArchive(fileTable.getSelectedRow());
				
				Archives archives = Archives.getInstance();
				archives.removeArchive(fileTable.getSelectedRow());
				String json = new Gson().toJson(archives.getArchives()).toString();
				db.updateData(json);
				
			}
		});
	}
	
	private void startService() {
		
		try {
			int interval = Integer.parseInt(txtInterval.getText());
			
			Archives a = Archives.getInstance();
			
			if (interval >= 5000) {
				agent = new NewAgent(a.getArchives(), Long.valueOf(interval));
				agent.setOnVerificationDoneListener(tableModel);
				agent.startVerificator();
				btnStatus.setBackground(Color.GREEN);
			} else {
				throw new NumberFormatException();
			}
		} catch (NumberFormatException e) {
			showMessage("Insira um valor valido para o intervalo de verificacao.\n"
					+ "So sao permitidos valores numericos acima de 5000.");
			txtInterval.requestFocus();
		}
		
		
		
		
	}

	public void startVerificator(long interval) {
		txtInterval.setText(String.valueOf(interval));
		startService();
		
	}
	
//	public static void main(String[] args) {
//		new Index().setVisible(true);
//	}
}
