package com.agente.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.agente.exceptions.InvalidDirectoryException;
import com.agente.model.Archive;
import com.agente.model.TransferContent;
import com.agente.negocio.Communicator;
import com.agente.negocio.Config;
import com.agente.repository.ArchiveRepository;
import com.agente.repository.IArchiveRepository;
import com.agente.util.Constantes;
import com.agente.util.Logger;
import com.agente.util.Util;

public final class ControllerArchieve {

	private static ControllerArchieve controllerArchieve;
	
	private IArchiveRepository repo = ArchiveRepository.getInstance();
	
	public ControllerArchieve() {

	}

	public static ControllerArchieve getInstance() {
		if (Util.objectIsNull(controllerArchieve)) {
			controllerArchieve = new ControllerArchieve();
		}
		 
		return controllerArchieve;
	}
	
	public Set<String> getExtensions(String directory) throws InvalidDirectoryException {
		
		File[] files = null;
		Set<String> extensions = new HashSet<String>();
		try {
			files = repo.listFiles(directory);
		} catch (InvalidDirectoryException e) {
			Logger.log(Constantes.CONTROLLER_ARCHIEVE_TAG, "error: " + e.getMessage());
			throw e;
		}
		
		String extension;
		if (!Util.objectIsNull(files)) {
			for (int i = 0; i < files.length; i++) {
				File file = files[i];
				extension = Util.getExtensionFile(file.getName());
				if (!Util.isNullOrEmpty(extension)) {
					extensions.add(extension);
				}
			}
		}
		
		return extensions;
	}
	
	public String validateData(String interval, String directory, String extensions) {
		
		StringBuilder errors = new StringBuilder();
		if (Util.isNullOrEmpty(interval)) {
			errors.append("Filed interval is required!\n");
		} else if (!Util.isNumeric(interval)) {
			errors.append("Value of the range field must be numeric!\n");
		}
		if (Util.isNullOrEmpty(directory)) {
			errors.append("Filed directory is required!\n");
		} else {
			File folder = new File(directory);
			if (!folder.exists()) {
				errors.append("Directory is invalid!\n");
			}
			File[] files = folder.listFiles();
			if (Util.objectIsNull(files) || files.length == 0) {
				errors.append("No files in the specified directory!\n");
			}
		}
		if (Util.isNullOrEmpty(extensions)) {
			errors.append("Filed extensions is required!\n");
		}
		
		return errors.toString();
	}
	
	public File[] listFiles(String directory) throws InvalidDirectoryException {
		
		File[] files = null;
		try {
			files = repo.listFiles(directory);
		} catch (InvalidDirectoryException e) {
			Logger.log(Constantes.CONTROLLER_ARCHIEVE_TAG, "error: " + e.getMessage());
			throw e;
		}
		
		return files;
	}
	
	
	public String sendContentToService(Archive archive) throws Exception {
		
		TransferContent transfer = createTrasnfer(archive);
		
		return Communicator.getInstance().post(transfer);
	}
	
	private TransferContent createTrasnfer(Archive archive) throws Exception {
		
		try {
			TransferContent transfer = new TransferContent();
			transfer.setServidorId(Config.getParametroAgente(Constantes.PAR_SERVER_ID)); //6f1df71e-47e9-4126-9836-05b0bf420e99
			transfer.setConteudo(Util.contentFromFile(archive.getPath()));
			transfer.setData(Calendar.getInstance());
			transfer.setServicoNome(Constantes.SERVICE_NAME);
			transfer.setUrl(Constantes.URI_SERVICE);
			return transfer;
		} catch (Exception e) {
			Logger.logInScreen("ERRO", "Arquivo nao enviado. Ocorreu um erro.");
		}
		
		
		return null;
	}
}
